LIBRARY ieee;
USE ieee.std_logic_1164.all;

ENTITY decoder IS
PORT(
	a : in std_logic;
	b : in std_logic;
	w : out std_logic;
	x : out std_logic;
	y : out std_logic;
	r : out std_logic
	);
END decoder;

	
architecture decoder_logic of decoder is
signal u1 : std_logic;
signal u2 : std_logic;
begin
			u1 <= not a;
			u2 <= not b;
			w <= u1 and u2;
			x <= u1 and b;
			y <= a and u2;
			r <= a and b;
end decoder_logic;
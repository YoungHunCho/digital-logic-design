LIBRARY ieee;
USE ieee.std_logic_1164.all;

ENTITY decoder_behavioral IS
PORT(
	a1 : in std_logic;
	a0 : in std_logic;
	y0 : out std_logic;
	y1 : out std_logic;
	y2 : out std_logic;
	y3 : out std_logic
	);
END decoder_behavioral;

	
architecture decoder_behavioral_logic of decoder_behavioral is
begin
	decoder: process(a1,a0)
begin
	if ((a1 or a0) = '0') then
		y0 <= '1';
	elsif((a1 and a0) = '1') then
			y3 <= '1';
	elsif((a1 and '1') = '1') then
			y2 <= '1';
	else
			y1 <= '1';
	end if;
end process decoder;
end decoder_behavioral_logic;
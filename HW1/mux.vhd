LIBRARY ieee;
USE ieee.std_logic_1164.all;

ENTITY mux IS
PORT(
	s1 : in std_logic;
	s0 : in std_logic;
	d0 : in std_logic;
	d1 : in std_logic;
	d2 : in std_logic;
	d3 : in std_logic;
	y : out std_logic
	);
END mux;

	
architecture mux_logic of mux is
signal ns0 : std_logic;
signal ns1 : std_logic;
signal a : std_logic;
signal b : std_logic;
signal c : std_logic;
signal d : std_logic;
begin
			ns0 <= not s0;
			ns1 <= not s1;
			a <= d0 and ns0 and ns1;
			b <= d1 and ns0 and s1;
			c <= d2 and s0 and ns1;
			d <= d3 and s0 and s1;
			y <= a or b or c or d;
end mux_logic;
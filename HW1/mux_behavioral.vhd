LIBRARY ieee;
USE ieee.std_logic_1164.all;

ENTITY mux_behavioral IS
PORT(
	s0 : in std_logic;
	s1 : in std_logic;
	d0 : in std_logic;
	d1 : in std_logic;
	d2 : in std_logic;
	d3 : in std_logic;
	y : out std_logic
	);
END mux_behavioral;

	
architecture mux_logic of mux_behavioral is
begin
	mux: process(s0, s1, d0, d1, d2, d3)
begin
	if ((s0 = '0') and (s1 = '0')) then
       y <= d0;
   elsif ((s0= '0') and (s1 = '1')) then
       y <= d1;
   elsif ((s0= '1') and (s1 = '0')) then
       y <= d2;
   else
       y <= d3;
   end if;
end process mux;
end mux_logic;